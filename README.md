# Hubs-Injection-Server. 

Runtime javascript injection for Mozilla Hubs.  
The main concept behind this is greatly derived from William Freeman's work (AKA colinfizgig on [github](https://github.com/colinfizgig)). 

We strongly recommend reading on and trying his injection method located here: https://github.com/colinfizgig/Custom-Hubs-Components.   

# Introduction.

This NodeJS server application allows for runtime javascript code injection in Mozilla Hubs amazing web based XR experience while keeping Mozilla Hubs client code modifications and rebuilds at a minimum. 

This explains not only the objectives and reasons behind using this, but it also is an attempt at providing a clear documentation on how to build quick components and how to use code injection in Mozilla Hubs projects. Along the way we will also explain how to use our simple storage solution and how to use the small socketIO sockets communication API we have added in the stack.        

Please take note:

* This is not official Mozilla documentation in any way.
* The Hubs-Injection-Server will only work for Hubs Cloud environments hosted on AWS and/or on a local environment. Do not try to set this up if you are creating Hubs experiences directly on https://hubs.mozilla.com/ it will not work.  

Simply put, we are sharing our methodology and processes with Hubs community members that might be interested in this approach. Who knows, someone might save a lot of time by reading this.  

## Platform. 
This stack works and has been tested on:   
- Ubuntu 18.0.4 LTS 
- Ubuntu 20.0.4 LTS
- Debian 11
- Raspbian Buster 5.10.17
- Windows 10
- OSX 10.15 Catalina
- OSX 11.4 Big Sur

## Main Dependencies. 

| Name | Version | Command | Description |
| ------ | ------ | ------ | ------ |
| [Git](https://git-scm.com/) | latest | OS specific | git is a decentralized / distibuted version control system |
| [NodeJS](https://nodejs.org/en/) | >= 10 | [See NodeJS installation instructions](https://github.com/nodesource/distributions/blob/master/README.md#debinstall) | JavaScript runtime | 
| NPM | >= 5.6.0 | Included with the NodeJS installation | JavaScript package manager 
| [MongoDB](https://www.mongodb.com/) | latest | [See MongoDB installation instructions](https://docs.mongodb.com/manual/installation/) | MongoDB is NoSQL database |

**NodeJS Internal Package Dependencies.**.  
(automatically installs with the initial _npm install_ command) 

 | Name | Version | Description |
| ------ | ------ | ------ |
| [body-parser](https://www.npmjs.com/package/body-parser) | >= 1.19.0 | Node.js body parsing middleware |
| [cookie-parser](https://www.npmjs.com/package/cookie-parser) | >= 1.4.5 | NodeJS HTTP Cookie handler |  
| [cors](https://www.npmjs.com/package/cors) | >= 2.8.5 | Node.js Cross Origin Requests middleware |
| [decache](https://www.npmjs.com/package/decache) | >= 4.6.0 | Deletes modules from node.js require() cache |
| [dotenv](https://www.npmjs.com/package/dotenv) | >= 8.2.0 | Loads environment variables from a .env file into process.env |
| [express](https://expressjs.com/) | >= 4 | NodeJS web and routing framework |
| [md5](https://www.npmjs.com/package/md5) | >= 2.3.0 | MD5 message hashing |
| [merge-files](https://www.npmjs.com/search?q=merge-files) | >= 0.1.2 | Small bundler that combines multiple files into one |
| [mongoose](https://www.npmjs.com/package/mongoose) | >= 5.12.8 | NodeJS MongoDB object modeling tool | 
| [socket.io](https://socket.io/) | >= 4 | Node.js realtime framework server |
| [selfsigned]() | >= 4.1.1 | Self signed certificates creation |

## Idiosyncrasies, pipeline, standardization and uniformity issues.  
The stack uses pure ES6 style syntax (without babel) but due to dependency problems is not using ES6 modules. Therefore, this is still considered a CommonJS project.  

Take note, at the moment, while the code can be considered as clean, we are in the process of picking an official syntax standard that will satisfy everyone on our team.  

You will also notice that the application is in "vanilla" javascript and does not use any javascript bundlers and automators, nor does it include a proper testing methodology. While we do use a library to bundle up our injected code in one file, it is nowhere close to good practice standards.  

No build and test automation processes are part of the project and no CI pipeline has been setup.  

**All of those issues will be tackled in a near future.**

*Functionnal exemples of the components we will show how to create in this document can be found in the static/exemples folder.*  
 


## What we will take you through.

1. [Setting up the Hubs-Injection-Server.](./wiki/SettingUpTheHubsInjectionServer.md)
2. [Setting up a Mozilla Hubs Custom Client in order to fetch and inject code.](./wiki/SettingUpTheMozillaHubsCustomClient.md)
3. [Setting up advanced Hubs Cloud admin options to allow code injection.](./wiki/SettingUpAdvancedHubsCloudAdminOptions.md)
4. [How to start development with the Hubs-Injection-Server.](./wiki/StartDevelopment.md)
5. [How to create injected components for Mozilla Hubs.](./wiki/HowToCreateInjectedComponent.md)
6. [How to share components on the network.](./wiki/NetworkedComponents.md) 
7. [How to save states and values to a database.](./wiki/SavingToDB.md)
8. [How to use communication channels.](./wiki/UsingCommChannels.md)  
  

| Next Page |
| ------ |
| [Setting up the Hubs-Injection-Server.](./wiki/SettingUpTheHubsInjectionServer.md) |

  

