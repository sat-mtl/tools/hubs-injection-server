# 8. Using communication channels  
Due to strong demands for inter-rooms communications we get from partners and customers, we have put together an API that allows us to easily subscribe and emit data to a message brokering service. For simplicity's sake, we have built one for you using socket.io . If you are looking for something with more horse power, you can look at what we have done and do the same with REDIS or with a combo of REDIS and socket.io .   

There are two main communication channel "types": 

- room  
With the room channel you can send data to room specific listeners.   
For exemple we can send data to one room in particular; to a list of rooms or to all rooms.   
First thing to do is to register the rooms you need to send from and listen from.  
In your code, whenever you know you have connected, call:  
`SAT.Utils.io.room.register();` to set your room as ready to send and receive.  

Then you can emit data to rooms with the following methods:  

**emit**  
Use the `SAT.Utils.io.room.emit(dataToSend)` to send data to the room you are currently in.  

**emitToRooms**  
Use the `SAT.Utils.io.room.emitToRooms([hubId1, hubId2, ...], dataToSend)` to send data to specific rooms. 

**emitToAllRooms**  
Use the `SAT.Utils.io.room.emitToAllRooms(dataToSend)` to send data to all rooms.  

You can listen for incoming room messages by subscribing to the event emission by inserting this in your room's code:  

```
SAT.Utils.io.room.on('IOMessage', (data)=> {
	console.log("received room message", data);
})
```  

Have a closer look at the event handling above, the eventName we receive is 'IOMessage'. That's our default eventName. If you want to specifiy another eventName when emitting, specify a `msgType` inside the dataObject you are sending. You would achieve this like this :   

`SAT.Utils.io.room.emit({'msgType':"ping", "someOtherData":"pong")`;  
And subscribe to it in this manner:  
```  

SAT.Utils.io.room.on('ping', (data)=> {
	console.log("received room message", data.someOtherData);
})
```   

- component  
Following the same concept as room, we can emit and subscribe based on components.    
First register your communication channel for the component:  
`SAT.Utils.io.component.register(componentID);` to set your component as ready to send and receive.    

**emit**  
Use the `SAT.Utils.io.component.emit(componentID, dataToSend)` to send data to the specified registered component channel.  

**emitToRoomComponents**  
Use the `SAT.Utils.io.room.emitToRoomComponents([componentID1, componentID2, ...], dataToSend)` to send data to speficied registered components inside the room.  

**emitToComponentAcrossRooms**  
Use the `SAT.Utils.io.room.emitToComponentAcrossRooms(componentID, dataToSend)` to emit to all registered components subscribed with the same ID accross rooms.  

**emitToAllComponents**  
Use the `SAT.Utils.io.room.emitToAllComponents(dataToSend)` to send data to all registered components, across rooms.  


You can listen for incoming component messages by subscribing to component event emissions by inserting this in your code:  

```
SAT.Utils.io.component.on('IOMessage', (data)=> {
	console.log("received component message", data);
})
```  

As in the room channel, you can also specify your own event name by using msgType inside the dataObject you emit.  





| Previous Page | Home |
| ------ | ------ | 
| [How to save states and values to a database.](./SavingToDB.md) | [index](../README.md) | 