# 3. Setting up advanced Hubs Cloud admin options. 

In order to load javascript files and other assets at runtime into the mozilla hubs client, we need to specify to Hubs what domain is allowed to load content so it can build it's cross domain policies.  

**This step is only required if your Hubs Cloud instance is running on a remote server.**
If you are working on a local Hubs Cloud instance, you can only call an injection server that is also running locally, therefore, there is no need for cross domain configurations for you. You can **SKIP THIS STEP** and only come back whenever you have deployed on a remote stack.  

One more thing before getting to the actual tasks at hand, when choosing a domain for your injection server, we've found you will save a lot of headaches by making your injection server address a simple subdomain of where your Hubs client is running.  

For exemple if your Hubs client is running at : https://myhubsclient.com then your injection server should be running at : https://injectionserver.myhubsclient.com. 

We cannot recommend this enough.  Choose wisely.
For the exemple below we will be using https://myhubsclient.com and https://injectionserver.myhubsclient.com,  
**Replace these with your own.**. 

Let's dive into this

Open up a browser and go to https://myhubsclient.com/admin

Go to Setup -> Server Settings


| Location | url or url template to insert |
| ------ | ------ |
| Extra Content Security Policy script-src Rules | https://*.myhubsclient.com/ |
| Extra Content Security Policy connect-src Rules | https://.myhubsclient.com/ wss://.myhubsclient.com/ |
| Extra Content Security Policy img-src Rules | https://injectionserver.myhubsclient.com |
| Extra Content Security Policy media-src Rules | https://injectionserver.myhubsclient.com |
| Extra Content Security Policy manifest-src Rules | https://injectionserver.myhubsclient.com |
| Extra Content Security Policy form-action Rules | https://injectionserver.myhubsclient.com |


Validate your insertions and press the SAVE button at the bottom of the page.  
  
  

| Previous Page | Home | Next Page |
| ------ | ------ | ------ |
| [Setting Up the Moziila Hubs Client](./SettingUpTheMozillaHubsCustomClient.md) | [index](../README.md) | [How to start development with the Hubs-Injection-Server.](./StartDevelopment.md ) |
