class NetworkedRectangleComponent {
    constructor(rectangleID, rectangleConfigs) {
        this.rectangleID = rectangleID;
        this.defaultConfigs =  {
            text_string:"DEFAULT TEXT",
            rectangle_width:2.5,
            rectangle_height:0.4,
            rectangle_color:'#550000',
            rectangle_position:'0 2 0',
            text_index:0,
            word_list:'famous|drip|uncle|awake|potato|jagged|secretary|license|stretch|shelf|explain|lie'
        }

        this.configs = {...this.defaultConfigs, ...rectangleConfigs};
        this.registerComponent(this.configs);
        this.buildTemplate("networkedrectangle-media");
        this.createEntity();
    }

    registerComponent(configs) {
        AFRAME.registerComponent('networkedrectanglecomponent', {
            schema: {
                rectangle_text_string: { type:'string', default:configs.text_string },
                rectangle_color: { type: 'color', default: configs.rectangle_color },
                rectangle_id:{type:'string', default: this.rectangleID},
                rectangle_width:{type:'number', default: configs.rectangle_width },
                rectangle_height:{type:'number', default: configs.rectangle_height },
                rectangle_position:{type:'string', default:configs.rectangle_position},
                text_index:{type:'number', default: configs.text_index },
                word_list:{type:'string', default:configs.word_list}
            },
            init:function() {
                let data = this.data;
                let el = this.el;
                this.update = this.update.bind(this);
                this.geometry = new THREE.PlaneBufferGeometry(data.rectangle_width, data.rectangle_height, 1, 1);
                this.material = new THREE.MeshBasicMaterial({ color: data.rectangle_color });
                this.material.transparent = true;
                this.material.opacity = 0.8;
                this.material.side = THREE.DoubleSide;
                
                // Create mesh.
                this.mesh = new THREE.Mesh(this.geometry, this.material);
                // Set mesh on entity.
                
                el.setObject3D('mesh', this.mesh);
                el.setAttribute('position', `${data.rectangle_position}`);
                
                NAF.utils
                    .getNetworkedEntity(this.el)
                    .then(networkedEl => {
                            
                        this.networkedEl = networkedEl;
                        this.networkedEl.setAttribute('text', 'value', `${data.rectangle_text_string}`);
                        this.networkedEl.setAttribute('text', 'align', 'center');
                        this.networkedEl.setAttribute('text', 'transparent', 'false');
                        this.networkedEl.setAttribute('text', 'wrapCount', 30);
                        this.networkedEl.setAttribute('text', 'width', 2.5);

                        this.networkedEl.object3D.addEventListener('interact', ()=> {
                            this.selectedRect(this.data, this.el);
                        });
                    });
            },
            update:function(oldData)
            {
                let doUpdate = async()=>
                {
                    let networkedEl = await NAF.utils.getNetworkedEntity(this.el);
                    this.networkedEl = networkedEl;
                    this.networkedEl.setAttribute('text', 'value', `${this.data.text_string}`);
                    this.networkedEl.setAttribute('text', 'align', 'center');
                    this.networkedEl.setAttribute('text', 'transparent', 'false');
                    this.networkedEl.setAttribute('text', 'wrapCount', 30);
                    this.networkedEl.setAttribute('text', 'width', 2.5);
                    await SAT.Utils.store.save("component", this.data.rectangle_id, this.data);
                }
                doUpdate();
               
            },
            selectedRect:function(data, el)
            {
                let doSelection = async()=>
                {
                    let rectNetworkElement = await NAF.utils.getNetworkedEntity(el);
                    if (!NAF.utils.isMine(rectNetworkElement))
                    {
                        NAF.utils.takeOwnership(rectNetworkElement);
                    }
                    let wordList = data.word_list.split("|");
                    let wordIndex = Number(data.text_index);
                    let word;
                    if (wordIndex < wordList.length-1)
                    {
                        rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_index', wordIndex+1);
                        word = wordList[wordIndex+1];
                        rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_string', word);
                    }
                    else
                    {
                        rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_index', 0);
                        word = wordList[0];
                        rectNetworkElement.setAttribute('networkedrectanglecomponent', 'text_string', word);
                    }
                     
                }
                doSelection();
            }
        });
    }

    buildTemplate(templateID) {
        let assets = document.querySelector("a-assets");
        let newTemplate = document.createElement("template");
        
        newTemplate.id = `${templateID}`;
        newTemplate.innerHTML = `<a-entity 
        class="interactable" 
        is-remote-hover-target 
        set-unowned-body-kinematic 
        tags="isHandCollisionTarget: false; isHoldable: false; offersHandConstraint: false; offersRemoteConstraint: false; inspectable: true; singleActionButton:true; isStatic: true;togglesHoveredActionSet: true;" 
        body-helper="type: static; mass: 1; collisionFilterGroup: 1; collisionFilterMask: 15;"
        hoverable-visuals 
        matrix-auto-update
        pinnable
        networkedrectanglecomponent
        >
        </a-entity>`;
        assets.appendChild(newTemplate);
        NAF.schemas.add({
            // template to add (created above)
            template: `#${templateID}`,
            components: [{
                    component: "position"
                },
                {
                    component: "rotation"
                },
                {
                    component: "scale"
                },
                "networkedrectanglecomponent", 
                {
                    component: "networkedrectanglecomponent",
                    attribute:"text_string"
                }, 
                {
                    component: "networkedrectanglecomponent",
                    attribute:"text_index"
                }
            ]
        });
    }

    createEntity() {
        return new Promise((resolve, reject) => { 
            let doCreate = async () => { 
                let rectangleEntity = document.createElement("a-entity");
                let loadedRectangle = new Promise((resolve, reject) => { rectangleEntity.addEventListener('loaded', resolve, { once: true }) });
                rectangleEntity.setAttribute("id", `${this.rectangleID}Entity`);
                rectangleEntity.setAttribute("networkedrectanglecomponent", `rectangle_id:${this.rectangleID};`);
                rectangleEntity.setAttribute('networked', {template: "#networkedrectangle-media", "persistent":true, "attachTemplateToLocal":true, "networkId":`${this.rectangleID}_netRect}` });
                document.querySelector('a-scene').appendChild(rectangleEntity);
                await loadedRectangle;
                
                return resolve(loadedRectangle);
            }
            doCreate();
        });
    }
}

let rectangle;
setTimeout(()=> {
    rectangle = new NetworkedRectangleComponent("rectangle1", null);
}, 
1000);