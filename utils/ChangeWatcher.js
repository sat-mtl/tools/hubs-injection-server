const EventEmitter  = require('events')
    , FS            = require('fs')
    , MD5           = require('md5')
    , Utils         = require('./DiskUtils');

class ChangeWatcher extends EventEmitter {
    static get FILE_CHANGED_EVENT() { return "fileChangedEvent"; }
    static get STARTED_WATCHING() { return "startedWatchingEvent"; }
    static get STOPPED_WATCHING() { return "stoppedWatchingEvent"; }
    
    constructor(path) {
        super();
        this.isWatching = false;
        this.md5Previous = "";
        this.md5Current = "";
        this.path = path;
        this.waitingTimer = null;
        this._watcher = null;
        this.startWatching();
    }
    
    startWatching() {
        return new Promise((resolve, reject) => {
            this._watcher = FS.watch(this.path, (event, filename) => {
                this.emit(ChangeWatcher.STARTED_WATCHING, this.path);
                
                if (filename) {
                    if (this.waitingTimer) return;
                    this.waitingTimer = setTimeout(() => {
                        this.waitingTimer = null;
                    }, 
                    3000);

                    Utils.FS_READ_FILE(this.path)
                    .then((res) => 
                    {
                        this.md5Current = MD5(res);
                        if (this.md5Current === this.md5Previous) {
                            return;
                        }
                        this.md5Previous = this.md5Current;
                        this.emit(ChangeWatcher.FILE_CHANGED_EVENT, this.path, res);
                        this.isWatching = true;
                        
                    })
                    .catch((err) => {
                        console.error(err);
                    });
                }
                return resolve(this.path);
            });
        })
    }

    stopWatching() {
        return new Promise((resolve, reject) => {
            this.isWatching = false;
            this.emit(ChangeWatcher.STOPPED_WATCHING, this.path);
            this._watcher.close();
            return resolve(this.path);
        });
    }
}

module.exports = ChangeWatcher;