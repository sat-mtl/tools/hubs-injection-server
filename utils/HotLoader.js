'use strict';

const Decache       = require('decache')
    , DiskUtils     = require('./DiskUtils')
    , EventEmitter  = require('events')
    , FS            = require("fs")
    , Path          = require('path')
    , Watcher       = require("./ChangeWatcher");
    


class HotLoader {
    static get moduleLoaded() { return "moduleLoadedEvent"; }
    static get moduleReloaded() { return "moduleReloadedEvent"; }
    static get moduleUnloaded() { return "moduleUnloadedEvent"; }
    static get moduleWillUnload() { return "moduleWillUnloadEvent"; }

    static get jsonFileLoaded() { return "jsonFileLoaded"; }
    static get jsonFileReloaded() { return "jsonFileReloaded"; }
    static get jsonFileUnloaded() { return "jsonFileUnloaded"; }

    static get eventEmitter() {  
        return this.hasOwnProperty('_eventEmitter') ? this._eventEmitter : void 0; 
    }

    static get hotLoadedJsonFiles() { 
        return this.hasOwnProperty('_hotLoadedJsonFiles') ? this._hotLoadedJsonFiles : void 0;
    }

    static get hotLoadedModules() { 
        return this.hasOwnProperty('_hotLoadedModules') ? this._hotLoadedModules : void 0;
    }

    static get watchers() { 
        return this.hasOwnProperty('_watchers') ? this._watchers : void 0;
    }

    static get optionalArgsMap() {
        return this.hasOwnProperty('_optionalArgs') ? this._optionalArgs : void 0;
    }


    static load(moduleFilePath, optionalArgs) {
        return new Promise((resolve, reject) => {
            const doLoadingTask = async()=> {
                try {
                    let isReload = false;
                    let path = Path.join(process.cwd(), moduleFilePath);
                    if (this.hotLoadedModules.has(path)) {
                        isReload = true;
                        await this.unload(path);
                    }

                    this.optionalArgsMap.set(path, optionalArgs);
                    
                    let module = require(path);
                    this.hotLoadedModules.set(path, module);
    
                    let watcher = new Watcher(path);
                    watcher.once(Watcher.FILE_CHANGED_EVENT, () => { 
                        HotLoader.load(moduleFilePath, this.optionalArgsMap.get(Path.join(process.cwd(), moduleFilePath))); 
                    });

                    this.watchers.set(path, watcher);
                    
                    if (isReload) {
                        this.eventEmitter.emit(this.moduleReloaded, module.module || module, optionalArgs);
                        if (module && module.moduleReloaded && typeof module.moduleReloaded == 'function') {
                            module.moduleReloaded(optionalArgs);
                        }
                    } else {
                        this.eventEmitter.emit(this.moduleLoaded, module.module || module, optionalArgs);
                        if (module && module.moduleLoaded && typeof module.moduleLoaded == 'function') {
                            module.moduleLoaded(optionalArgs);
                        }
                    }
    
                    // isReload ? console.log("Module reloaded ") : console.log("Module should be loaded ");
                    if (module) {
                        return resolve(module.module || module, optionalArgs);
                    }
                }
                catch(err) {
                    console.warn(err);
                    return reject(err);
                }
            }; 
            doLoadingTask();
        }); 
    }

    static unload(moduleFilePath) {
        return new Promise((resolve, reject) => {
            const doUnloadingTask = async()=> {
                try {
                    let path = require.resolve(moduleFilePath);
                    let watcher = this.watchers.get(path);
                    
                    watcher.removeAllListeners(Watcher.FILE_CHANGED_EVENT);
                    await watcher.stopWatching();
                    this.watchers.delete(path);
                    watcher = null;

                    let module = this.hotLoadedModules.get(path);
                    if (module && module.moduleWillUnload && typeof module.moduleWillUnload == 'function') {
                        module.moduleWillUnload();
                    }
                    this.eventEmitter.emit(this.moduleWillUnload, path);
                
                    Decache(path);
                    this.eventEmitter.emit(this.moduleUnloaded, path);
                    return resolve();
                } catch(err) {
                    return reject(err);
                }
            };

            doUnloadingTask();
        });
    }


    static loadJSON(jsonFilePath, locale) {
        return new Promise((resolve, reject) => {
            const doLoadingTask = async()=> {
                try {
                    let isReload = false;

                    let path = Path.join(process.cwd(), jsonFilePath);
                    if (this.hotLoadedJsonFiles.has(path)){
                        isReload = true;
                        await this.unloadJSON(path, locale);
                    }

                    let json = await DiskUtils.FS_READ_FILE(path);
                    json = JSON.parse(json);

                    this.hotLoadedJsonFiles.set(locale, json);
        
                    let watcher = new Watcher(path);
                    watcher.once(Watcher.FILE_CHANGED_EVENT, () => { 
                        let loc = jsonFilePath.substring(jsonFilePath.lastIndexOf("/")+1).split(".")[0];
                        console.log(loc);
                        HotLoader.loadJSON(jsonFilePath, loc); 
                    });

                    this.watchers.set(path, watcher);
                    
                    if (isReload) {
                        this.eventEmitter.emit(this.jsonFileReloaded, json, locale);
                    } else {
                        this.eventEmitter.emit(this.jsonFileLoaded, json, locale);
                    }
    
                    // isReload ? console.log("Module reloaded ") : console.log("Module should be loaded ");
                    if (json) {
                        return resolve(json);
                    }
                } catch(err) {
                    console.warn(err);
                    return reject(err);
                }
            };
            doLoadingTask();
        }); 
    }

    static unloadJSON(jsonFilePath, locale) {
        return new Promise((resolve, reject) => {
            const doUnloadingTask = async()=> {
                try {
                    let path = require.resolve(jsonFilePath);
                    let watcher = this.watchers.get(path);
                    
                    watcher.removeAllListeners(Watcher.FILE_CHANGED_EVENT);
                    await watcher.stopWatching();
                    this.watchers.delete(path);
                    watcher = null;
                    
                    this.hotLoadedJsonFiles.delete(locale);
                    this.eventEmitter.emit(this.jsonFileUnloaded, path);
                    return resolve();
                } catch(err) {
                    console.warn(err);
                    return reject(err);
                }
            };
            doUnloadingTask();
        });
    }
}

HotLoader._eventEmitter = new EventEmitter();
HotLoader._hotLoadedModules = new Map();
HotLoader._hotLoadedJsonFiles = new Map();
HotLoader._watchers = new Map();
HotLoader._optionalArgs = new Map();
module.exports = HotLoader;
